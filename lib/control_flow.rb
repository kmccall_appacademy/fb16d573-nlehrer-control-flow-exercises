# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.select { |el| el.upcase == el }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  len = str.length
  str.length.odd? ? str[len / 2] : str[len / 2 - 1..len / 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  VOWELS.map { |vow| str.count(vow) }.inject(:+)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ''
  arr.each { |el| str << (el + separator) }
  str = str[0..-2] if str[-1] == separator
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.each_with_index.map { |c, i| i.odd? ? c.upcase : c.downcase }.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map { |word| word.length >= 5 ? word.reverse : word }.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |el|
    if (el % 15).zero?
      'fizzbuzz'
    elsif (el % 3).zero?
      'fizz'
    elsif (el % 5).zero?
      'buzz'
    else
      el
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr_rev = []
  arr.each { |el| arr_rev.unshift(el) }
  arr_rev
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return true if num == 2
  return false if num == 1 || num.even?
  (3..num / 2).step(2) { |x| return false if (num % x).zero? }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |x| (num % x).zero? }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (1..num).select { |x| (num % x).zero? && prime?(x) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  arr.select(&:odd?).length == 1 ? arr.select(&:odd?)[0] : arr.select(&:even?)[0]
end
